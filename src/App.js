import React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom"; // Redirect

//import Index from "views/Index.js";  
import Landing from "views/examples/Landing.js";
//import Login from "views/examples/Login.js";
//import UserProfile from "views/examples/Profile.js";
//import Register from "views/examples/Register.js";

//import DemoNavbar from "components/Navbars/DemoNavbar.js";

// import ModalJitsi from "./components/JitsiComp";



export default function App(){
    //const [isStreamOpen, setIsStreamOpen] = useState(false);

    
    return(
        <>           
            <BrowserRouter>
                {/* <DemoNavbar setIsStreamOpen={(e) => setIsStreamOpen(e)} /> */}
                <Switch>  
                    {/* Landing page */ }                                
                    {/* <Route
                        path="/"
                        exact
                        component={Landing}
                    />  */}
                    {/* End Landing page */ }                      
                    <div>
                        <DemoNavbar setIsStreamOpen={(e) => setIsStreamOpen(e)} />
                        <Route path="/" exact render={props => <Index {...props} />} />
                        <Route path="/login-page" exact render={props => <Login {...props} />} />
                        <Route
                            path="/profile-page"
                            exact
                            render={props => <UserProfile {...props} />}
                        />
                        <Route
                            path="/register-page"
                            exact
                            render={props => <Register {...props} />}
                        />
                        <Redirect to="/" />
                    </div>              
                </Switch>           
            </BrowserRouter>
            {/* <ModalJitsi isOpened={isStreamOpen} closeCallback={(e) => setIsStreamOpen(e)} /> */}
        </>
    )

}