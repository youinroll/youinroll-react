import React from "react";

import LazyHero from 'react-lazy-hero';
import {Button} from "reactstrap";
import Particles from 'react-particles-js';
import {particleParams} from "./particles";


const PStyle = () =>{

  return(
      <style type="text/css">
        {`
        .part-div {
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 0;
          minHeight: 100vh;
        }
        
        .btn-coral {
          background-color: #fe2c55;
          color: #fff;
        }
        `}
    </style>
  )
}


const CoolParticles = () => {
  return (  
    <>
      <PStyle />
      <div className="part-div d-none d-sm-block">      
        <Particles
            params={particleParams[1]} 
        />
      </div>
    </>
  )
}

export default function HeroStart() {
      return (
          <>              
            <CoolParticles />           
              <div> 
                <LazyHero
                    //parallaxOffset={100}
                    //imageSrc="react/land2.jpg"
                    // color="#fbf2f6"
                    opacity={0.1}
                      minHeight="100vh"
                > 
                    <div className="mb-4 px-2">                      
                      <strong>
                        <h1 className="text-dark display-2 mb-4">
                          Делитесь знаниями с помощью видео
                        </h1>
                        <h2 className="text-dark display-3">
                          Образовательные видео решения
                        </h2>
                      </strong>
                    </div>                     
                    <Button                         
                      variant="success" 
                      className="btn-coral"
                      href="http://www.youinroll.com/register"
                      onClick={() => window.location.reload()}
                    >
                      Присоединиться
                    </Button>
                </LazyHero>                
            </div>
          </>
      );
  };


/* 
export default function HeroStart(){


    return(

        <div className="position-relative">
           
            <section className="section section-lg section-shaped pb-250 pt-20 mt-0 ">            
              <div className="shape shape-style-1 shape-default alpha-4">
                <img 
                  src={'react/land.jpg'} 
                  alt="asas" 
                  width="100%" 
                  opacity="50"
                />                
              </div>
              <Container className="py-lg-md d-flex">
                <div className="col px-0">
                  <Row >
                    <Col lg="6">
                      <h1 className="display-3 text-white">
                        YouInRoll{" "}
                        <span>Социальная сеть для учащихся, учителей и родителей</span>
                      </h1>
                      <p className="lead text-white">
                        Открой для себя безграничные знания 
                      </p>
                      <div className="btn-wrapper">
                        <Button
                          className="btn-icon mb-3 mb-sm-0"
                          color="info"
                          href="https://demos.creative-tim.com/argon-design-system-react/#/documentation/alerts?ref=adsr-landing-page"
                        >
                        {/*   <span className="btn-inner--icon mr-1">
                            <i className="fa fa-code" />
                          </span> 
                          <span className="btn-inner--text">Видео о нас</span>
                        </Button>
                        <Button
                          className="btn-white btn-icon mb-3 mb-sm-0 ml-1"
                          color="default"
                          href="https://www.creative-tim.com/product/argon-design-system-react?ref=adsr-landing-page"
                        >
                          <span className="btn-inner--icon mr-1">
                            <i className="ni ni-cloud-download-95" />
                          </span> 
                          <span className="btn-inner--text">
                            Регистрация
                          </span>
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Container>
            
            </section>
            
          </div>
    )
} */