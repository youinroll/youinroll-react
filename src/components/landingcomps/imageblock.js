import React from "react";

import {    
    Container,
    Row,
    Col
  } from "reactstrap";


export default function ImageBlock({heading, cooltext, imag, isright, textunderimg, showSm}){

  const sectClassName = showSm ? 'd-none d-sm-block' : "";

    return(
    <section className={"section section-lg " + sectClassName }>
        <Container fluid>
          <Row className="row-grid align-items-center">
            {isright && <Col className="d-none d-sm-block" md="2">              
            </Col>}
            <Col className={isright ? `order-md-2` : `order-md-1`} md="6">
                <div className="rounded shadow-lg overflow-hidden">
                  <img src={imag} alt="some text" style={{ width: '100%' }}/>
                </div>
                <div className="pt-4">
                  <p className="h5">
                    {textunderimg}
                  </p>   
                </div>            
            </Col>
            <Col className={isright ? `order-md-1` : `order-md-2`} md="4">
              <div className={isright ? `pr-md-5` : `pl-md-5`}>                
                <h3 className="display-3">{heading}</h3>
                <p className="lead">
                  {cooltext}
                </p>                
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    )
}