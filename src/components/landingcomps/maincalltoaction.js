import React from "react";
// reactstrap components
import {
    //Badge,
    Button,
    Card,
   // CardBody,
   // CardImg,
   // FormGroup,
   // Input,
   // InputGroupAddon,
   // InputGroupText,
  //  InputGroup,
    Container,
    Row,
    Col
  } from "reactstrap";
  


export default function MainCallToAct(){

    return(
        <section className="section section-lg pt-0">
            <Container>
              <Card className="bg-gradient-default shadow-lg border-0">
                <div className="p-5">
                  <Row className="align-items-center">
                    <Col lg="8">
                      <h3 className="text-white">
                        Образование будет проще и лучше
                      </h3>
                      <p className="lead text-white mt-3">
                        Попробуйте наши сервисы в тестовом режиме на 30 дней.
                      </p>
                    </Col>
                    <Col className="ml-lg-auto" lg="3">
                      <Button
                        block
                        className="btn-white"
                        color="default"
                        href="https://youinroll.com/register?backurl="
                        size="lg"
                      >
                        Регистрация
                      </Button>
                    </Col>
                  </Row>
                </div>
              </Card>
            </Container>
          </section>
    )
}