import React from "react";

// reactstrap components


// reactstrap components
import {
    Badge,
    Container,
    Row,
    Col,
    UncontrolledCarousel
  } from "reactstrap";


  const items = [
    {
      src: "react/ed2.jpg",
      altText: "",
      caption: "",
      header: ""
    },
    {
      src: "react/ed1.jpg",
      altText: "",
      caption: "",
      header: ""
    },
    {
      src: "react/ed4.jpg",
      altText: "",
      caption: "",
      header: ""
    }
  ];

export default function OurServices(){


    return(      
      <section className="section section-lg">
        <Container>
          <Row className="row-grid align-items-center">
            <Col className="order-md-2" md="6">
                <div className="rounded shadow-lg overflow-hidden transform-perspective-right">
                  <UncontrolledCarousel items={items} />
                </div>              
            </Col>
            <Col className="order-md-1" md="6">
              <div className="pr-md-5">
                <div className="icon icon-lg icon-shape icon-shape-default shadow rounded-circle mb-5">
                  <i className="ni ni-settings-gear-65" />
                </div>
                <h3 className="display-3">Наши сервисы</h3>
                <p>
                  Мы полностью российская социальная сеть, патриоты и любители своего дела. Мы предлагаем:
                </p>
                <ul className="list-unstyled mt-5">
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <Badge
                          className="badge-circle mr-3"
                          color="default"
                        >
                          <i className="ni ni-settings-gear-65" />
                        </Badge>
                      </div>
                      <div>
                        <h6 className="mb-0">
                          Лучшие видеоконференции
                        </h6>
                      </div>
                    </div>
                  </li>
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <Badge
                          className="badge-circle mr-3"
                          color="default"
                        >
                          <i className="ni ni-html5" />
                        </Badge>
                      </div>
                      <div>
                        <h6 className="mb-0">Бесконечные стримы</h6>
                      </div>
                    </div>
                  </li>
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <Badge
                          className="badge-circle mr-3"
                          color="default"
                        >
                          <i className="ni ni-satisfied" />
                        </Badge>
                      </div>
                      <div>
                        <h6 className="mb-0">
                          Инструменты для создания онлайн курсов
                        </h6>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    )
}