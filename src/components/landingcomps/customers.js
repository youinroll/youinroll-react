import React from "react";

// reactstrap components
import {   
    Card,    
    CardImg,   
    Container,
    Row,
    Col
  } from "reactstrap";



export default function OurCustomers(){

    return(
        <section className="section">
            <Container>
              <Row className="row-grid align-items-center">
                <Col md="6">
                  <Card className="bg-default shadow border-0">
                    <CardImg
                      alt="..."
                      src={'react/fire.jpg'} 
                      top
                    />
                    <blockquote className="card-blockquote">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="svg-bg"
                        preserveAspectRatio="none"
                        viewBox="0 0 583 95"
                      >
                        <polygon
                          className="fill-default"
                          points="0,52 583,95 0,95"
                        />
                        <polygon
                          className="fill-default"
                          opacity=".2"
                          points="0,42 583,95 683,0 0,95"
                        />
                      </svg>
                      <h4 className="display-3 font-weight-bold text-white">
                        Профессионалы профессионалам
                      </h4>
                      <p className="lead text-italic text-white">
                        Постройте свою образовательную среду на нашей платформе и зарабатывайте с нами. 
                      </p>
                    </blockquote>
                  </Card>
                </Col>
                <Col md="6">
                  <div className="pl-md-5">
                    <div className="icon icon-lg icon-shape icon-shape-default shadow rounded-circle mb-5">
                      <i className="ni ni-building text-default" />                     
                    </div>
                    <h3 className="display-3">Организациям</h3>
                    <p className="lead">
                      Устали от 40-минутных звонков в Зуме? Хотите отечественную платформу для курсов?
                    </p>
                    <p>
                      Мы поможем Вам трансформировать Вашу образовательную среду в цифровой формат
                    </p>
                    <p>
                      Ваши сотрудники и ученики будут сфокусированы на передаче и получении знаний, а не на настройке оборудования.
                    </p>
                    <a
                      className="font-weight-bold text-default mt-5"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      Наша платформа соответсвует всем стандартам по передаче и хранению данных согласно российскому законодательству.
                    </a>
                  </div>
                </Col>
              </Row>
            </Container>
          </section>
    )
}