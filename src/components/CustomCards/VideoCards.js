import React from "react";
// nodejs library that concatenates classes
import PropTypes from "prop-types";

// reactstrap components
import {  
  Card,
  CardBody,
  Container,
  Row,
  Col  
} from "reactstrap";





const YoutubeEmbed = ({ embedId }) => (
  <div className="video-responsive" width="100%">
    <iframe
      width="100%"
    //  height="480"
      src={`https://www.youtube.com/embed/${embedId}`}
      frameBorder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
      title="Embedded youtube"
    />
  </div>
);

YoutubeEmbed.propTypes = {
  embedId: PropTypes.string.isRequired
};


const keysss = [0,1,2,3,4];
const videos = [
  'pRpvdcjkT3k',
  'Te4wx4jtiEA',
  'efTj6UYzvk4',
  'B39L_tu0PjE',
  'B39L_tu0PjE',
  'B39L_tu0PjE'
]


export default function VideoCards () {
  
    return (        
        <Container>
          <Row className="justify-content-center py-4">
            <Col lg="12">
              <Row className="row-grid">
                {keysss.map((kee) => 
                  <Col key={kee} lg="4">
                    <Card bg="light" className="border-0 my-3">
                      <YoutubeEmbed embedId={videos[kee]}   />
                      <CardBody className="pt-0 px-1 pb-1 mt-0" >
                        <Row>
                          <Col xs="3" className="mx-0 pr-1 pt-2 ">
                            <img className="rounded-circle" src={`https://picsum.photos/id/3${kee}/50/55`} alt="dasda" width="75%" />
                          </Col>
                          <Col xs="9" className="pl-1 pr-0 pb-0 pt-2">                         
                            <h6 className="mb-0">AОчень длинное название</h6>
                            <p className="description my-0">
                              Урок как спасти мир
                            </p>
                            <p className="description text-muted mt-0 mb-0" >
                              Какая-то хрень видимо это теги
                            </p>
                          </Col> 
                        </Row> 
                      </CardBody>                   
                    </Card>
                  </Col> 
                )}



                              
              </Row>
            </Col>
          </Row>
        </Container>      
    );  
}

