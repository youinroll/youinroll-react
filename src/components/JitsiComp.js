import React, { useMemo } from "react";
// nodejs library that concatenates classes
// reactstrap components
import {Button, Spinner} from "reactstrap";

import Jitsi from 'react-jitsi';
import Draggable from 'react-draggable';

// border: 3px solid #73AD21;

const DownSty = () => <style type="text/css">
                    {`
                    .fixed-div {
                        position: fixed;
                        bottom: 0;
                        right: 0;
                        width: 400px;
                        height: 500px;
                        z-index: 1000
                    }
                    `}
                </style>;

const LoadingComponent = () =>{
    return(
        <>
            <DownSty/>
            <Spinner
                    animation="grow"
            />
           
        </>
    )
}

function JitsiCustom ()  {
    //const [roomName, setRoomName] = useState("someroom");
    //const [userFullName, setUserFullName] = useState("Somename");

  
    return (
      <Jitsi
        config={{ startAudioOnly: true }}
        interfaceConfig={{ filmStripOnly: true }}
        domain="smartfooded.com"
        roomName="dadsas"
        containerStyle={{ width: '400px' }}
        loadingComponent={LoadingComponent}
        displayName="Mishka Kkkk"
        email="dasds@fdsfds.com"        
      />
    )
  }


export default function ModalJitsi({isOpened, closeCallback}) {

    // const [minimized, setMinimize] = useState(false);

    const jits = useMemo(() => <JitsiCustom />, []);
    
    if (!isOpened){
        return ""
    };
    
    return(
        <>
            <DownSty />
            <Draggable
                axis="both"                
                handle=".handle"
                defaultPosition={{x: 0, y: 0}}
                position={null}
                grid={[20, 20]}
                scale={1}
            //    onStart={this.handleStart}
            //   onDrag={this.handleDrag}
            //  onStop={this.handleStop}
            >
                <div className="fixed-div">
                    <div className="row">
                        <div className="col-10 pr-1">
                            <div className="btn btn-info btn-lg btn-block handle">
                                Перетащи меня        
                            </div>  
                        </div>
                        <div className="col-2 pl-1">
                            <Button 
                                className="m-0"
                                size="lg"
                                variant="danger" 
                                block
                                onClick={() => closeCallback(false)}
                            >
                                x    
                            </Button>  
                        </div>
                    </div>
                                    
                    {jits}                    
                </div>
            
            </Draggable>
        </>
    )    
}
