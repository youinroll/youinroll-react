
import React from "react";

// reactstrap components
import { Card, Container } from "reactstrap";

// core components

import SimpleFooter from "components/Footers/SimpleFooter.js";

// import TabsSection from "../IndexSections/Tabs";

import axios from "axios";

import VideoCards from "../../components/CustomCards/VideoCards";

import {  
  Grid,
 // Photo,
  GridControlBar,
  GridControlBarItem,
  Profile } from "react-instagram-ui-kit";


const samplePhotos = () =>{
  let phArr = [];
  let i;
  for (i = 0; i < 10; i++) {
    axios.get(`https://picsum.photos/id/${i}/info`)
    .then((res) => {     
      phArr.push(res.data);
      return phArr
    })
    .catch((err) =>  console.log(err))   
  } 
  console.log(phArr);
  return ""
  //return phArr
}




class UserProfile extends React.Component {
  componentDidMount() {
    const phts = samplePhotos();
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    this.refs.main.scrollTop = 0;
    this.setState({
      avatarUrl: "https://youinroll.com/res.php?src=storage/uploads/d1882293076e6e91c230bb2fecba82e9-1.jpg&q=100&w=130&h=130",
      bgBanner: "https://youinroll.com/storage/uploads/98ccb39ea4855732d019c98aa8f54a62-1.jpg",
      userName: "Никита Вадимович",
      userDescription: null,
      userFollowers: 67,
      userVidCount: 677,
      userVidWatch: 36900,
      userHasLikes: 8200,
      examplePhotos: phts
    })
    
  }
  render() {    
   
    return (
      <>        
        <main className="profile-page" ref="main">
          <section className="section-profile-cover section-shaped my-0">
            {/* Circles background */}
            <div className="shape shape-style-1 shape-default alpha-4">
             <img src={this.state && this.state.bgBanner && this.state.bgBanner} alt="sa" height="100%" />              
            </div>
            {/* SVG separator */}
            <div className="separator separator-bottom separator-skew">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                preserveAspectRatio="none"
                version="1.1"
                viewBox="0 0 2560 100"
                x="0"
                y="0"
              >
                <polygon
                  className="fill-white"
                  points="2560 0 2560 100 0 100"
                />
              </svg>
            </div>
          </section>
          <section className="section">
            <Container>
              <Card className="card-profile shadow mt--300">
                  
                <div className="px-3">
                  <Profile
                    bio={`
                      Отец основатель youinroll, преподаватель
                    `}
                    pictureSrc={this.state && this.state.avatarUrl && this.state.avatarUrl}
                    username="xatikont"
                    fullname="Никита Вадимович"
                    followersData={[31, 3000000, 55]}
                  />
                  <Grid>
                    <GridControlBar>
                      <GridControlBarItem isActive>𐄹 Видео</GridControlBarItem>
                      <GridControlBarItem>웃 Трансляции</GridControlBarItem>
                    </GridControlBar>
                    <VideoCards />
                   {/*  {this.state && this.state.examplePhotos &&
                    <>
                      {this.state.examplePhotos.map(photo => {
                        console.log(photo);
                        return (<Photo src={photo.download_url} key={photo.id} />)
                        })}
                    </>
                    } */}
                  </Grid>
                  
{/* 
                  <Row className="justify-content-left">
                    <Col className="order-lg-2" md="3">
                      <div className="card-profile-image">
                        <a href="#pablo" onClick={e => e.preventDefault()}>
                          <img
                            alt="..."
                            className="rounded-circle"
                            src={this.state && this.state.avatarUrl && this.state.avatarUrl}
                          />
                        </a>
                      </div>
                    </Col>
                    <Col
                      className="order-lg-3 text-lg-right align-self-lg-center"
                      md="4"
                    >
                      <div className="card-profile-actions py-4 mt-lg-0">
                        <Row>
                          <Col xs="6">
                            <Button
                              className="float-right"
                              color="primary"
                              href="#pablo"
                              onClick={e => e.preventDefault()}
                              size="sm"
                            >
                              Подписаться
                            </Button>                            
                          </Col>                          
                          <Col xs="3">
                            <Button
                              className="btn-icon btn-2 ml-1"
                              color="info"                              
                              type="button"
                              size="sm"
                            >
                              <span className="btn-inner--icon">
                                <i className="ni ni-bell-55" />
                              </span>
                            </Button>
                          </Col>
                          <Col xs="3">
                            <Button
                              className="btn-icon btn-2 ml-1"
                              color="danger"                            
                              type="button"
                              size="sm"
                            >
                              <span className="btn-inner--icon">
                                <i className="ni ni-notification-70" />
                              </span>
                            </Button>
                          </Col>           
                        </Row>
                      </div>
                    </Col>
                    <Col className="order-lg-1" md="4">
                      <div className="card-profile-stats d-flex justify-content-center">
                        <div>
                          <span className="heading">{this.state && this.state.userFollowers && this.state.userFollowers}</span>
                          <span className="description">Следят</span>
                        </div>
                        <div>
                          <span className="heading">{this.state && this.state.userVidCount && this.state.userVidCount}</span>
                          <span className="description">Видео</span>
                        </div>
                        <div>
                          <span className="heading">{this.state && this.state.userVidWatch && this.state.userVidWatch}</span>
                          <span className="description">Просмотров</span>
                        </div>                        
                      </div>
                    </Col>
                  </Row> 
                  <div className="text-center mt-5">
                    <h4>
                      {this.state && this.state.userName && this.state.userName}      
                    </h4>                    
                  </div>
                  <div className="mt-3 py-4 border-top text-center">                    
                    <TabsSection />
                  </div>     */}             
                </div>
                
              </Card>
            </Container>
          </section>
        </main>
        <SimpleFooter />
      </>
    );
  }
}

export default UserProfile;
