import React from "react";
import '@front10/landing-page-book/dist/components/Button/style.css';

import HeroStart from "../../components/landingcomps/herostart";
//import { Navbar } from '@front10/landing-page-book/dist/components';
import ImageBlock from "../../components/landingcomps/imageblock";
//import {Button} from "reactstrap";
import Button from '@front10/landing-page-book/dist/components/Button';

import {  
  Navbar
} from 'reactstrap';



import './custom-theme.css';

export default function Landing(){

  document.cookie = `landing=visited;max-age=3000000;domain=youinroll.com`;

  

 // customStyle.bgColor="#121212CC"

  return(
    <> 
      <style>
        {`
        .pinkcolor {          
          background: linear-gradient(#fbf2f6 20%, #fcf6f9, #ffffff);
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        .bluecolor {          
          background-color: #9cb1c4;          
        }

        .h1 {
          color: white;
          font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        }

        .header-color {
          background-color: #121212CC;
          backdrop-filter: blur(40px);
          width: 100%;
          height: 62px;
          position: fixed;
          top: 0px;
          z-index: 2000;
          justify-content: start;
        }

         
        .btn-coral {
          background-color: #fe2c55;
          color: #fff;

        }

        .logo-imgs {
          height: 30px;
          display: inline;
          margin-left: 10px;
        }

        `}
      </style>
      
      <main  className="pinkcolor">

        <Navbar          
          className="header-color"         
        >
          <img src="https://youinroll.com/lib/favicos/favicon.svg" className="logo-imgs" alt="treugolnik"/>
          <img src="https://youinroll.com/storage/uploads/logo-novoe-1-png60a247e5cb3d7.png" alt="textikkk" className="logo-imgs" />
        </Navbar>

       
        
          {/* <Navbar         
            brandLogo="https://youinroll.com/storage/uploads/logo-novoe-1-png60a247e5cb3d7.png"
            brandName="youinroll"
            fixed={true}
            bgColor={"dsadsa"}
            style={customStyle}  
          />   */}    
       
        <HeroStart /> 
        <ImageBlock 
          heading="Покажи и расскажи"
          cooltext="Готовьте и представляйте уроки, лекции и учебные материалы как можно более увлекательно.
           YouInRoll Запись позволяет записывать ваш экран, веб-камеру или и то, и другое вместе, так что вы можете передавать знания с личным подходом"
          imag={'land/less1.jpeg'}
          isright={true}
          
        />
        <ImageBlock 
          heading="Храните и управляйте"
          cooltext="Храните, управляйте и проводите видеолекции и прямые трансляции в
           самом высоком качестве — все это в безопасной, удобной для поиска библиотеке для вас и вашей аудитории."
          imag={'land/less2.jpeg'}
          showSm
          //textunderimg="Храните, управляйте и проводите видеолекции и прямые трансляции в
          //самом высоком качестве — все это в безопасной, удобной для поиска библиотеке для вас и вашей аудитории."       
        />
        <ImageBlock 
          heading="Проводите занятия публично или в частном порядке"
          cooltext="Встраивайте плеер на свой сайт и устанавливайте разрешения на просмотр, 
          защищайте паролем свои видео и прямые трансляции или делайте их эксклюзивными для вашей сети."
          imag={'land/less3.jpeg'}
          isright={true}
          showSm   
        />
        <ImageBlock 
          heading="Безопасно транслируйте высококачественный контент"
          cooltext="Будь то ваша следующая лекция, новая инициатива или просто сообщение студентам или преподавателям, 
          транслируйте события с надежным, кристально чистым живым видео любому зрителю в любом месте."
          imag={'land/stream.jpg'}
          showSm              
        />
        <ImageBlock 
          heading="Аналитика"
          cooltext="Получите видеоаналитику в режиме реального времени о том,
           сколько людей смотрят, где и на каких устройствах, чтобы ваши видео и прямые трансляции работали лучше для вашей аудитории"
          imag={'land/less4.jpeg'}
          isright={true} 
          showSm      
        />
        <div className="bluecolor" style={{ color: "white", fontFamily: "Helvetica Neue" }}>
          <div className="pt-4 d-flex justify-content-center bluecolor">
            <p className="h1 text-center">Создание глобальной сети учителей</p>
          </div>
          <div className="py-4 d-flex justify-content-center bluecolor">
            <h2 className="text-center" style={{ color: "white", fontFamily: "Helvetica" }}>Начни учиться</h2>
          </div>
          <div className="pb-6 d-flex justify-content-center" style={{ fontFamily: "Helvetica" }}>
            <Button                         
              variant="success"
              href="https://youinroll.com/register"
              className="btn-coral"
              onClick={() => window.location.reload()}
            >
              Присоединиться
            </Button>
          </div>
        </div>
      </main>
    </>
  )

}
