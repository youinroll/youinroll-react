import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";


 


// reactstrap components
import {
  Card,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
  Row,
  Col
} from "reactstrap";

// video card components
import VideoCards from "../../components/CustomCards/VideoCards";



class TabsSection extends React.Component {
  state = {
    iconTabs: 1,
    plainTabs: 1
  };
  toggleNavs = (e, state, index) => {
    e.preventDefault();
    this.setState({
      [state]: index
    });
  };
  render() {
    return (
      <>
        <h3 className="h4 text-success font-weight-bold mb-4">Информация автора</h3>
        <Row className="justify-content-center">
          <Col lg="12">
            {/* Tabs with icons */}
            <div className="mb-3">
              <small className="text-uppercase font-weight-bold">
                Во вкладках вы найдете что для вас подготовил Никита Вадимович
              </small>
            </div>
            <div className="nav-wrapper">
              <Nav
                className="nav-fill flex-column flex-md-row"
                id="tabs-icons-text"
                pills
                role="tablist"
              >                
                <NavItem>
                  <NavLink
                    aria-selected={this.state.iconTabs === 1}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.iconTabs === 1
                    })}
                    onClick={e => this.toggleNavs(e, "iconTabs", 1)}
                    href="#pablo"
                    role="tab"
                  >
                    <i className="ni ni-badge mr-2" />
                    Описание
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.iconTabs === 2}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.iconTabs === 2
                    })}
                    onClick={e => this.toggleNavs(e, "iconTabs", 2)}
                    href="#pablo"
                    role="tab"
                  >
                    <i className="ni ni-camera-compact mr-2" />
                    Видео
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.iconTabs === 3}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.iconTabs === 3
                    })}
                    onClick={e => this.toggleNavs(e, "iconTabs", 3)}
                    href="#pablo"
                    role="tab"
                  >
                    <i className="ni ni-image mr-2" />
                    Изображения
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.iconTabs === 4}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.iconTabs === 4
                    })}
                    onClick={e => this.toggleNavs(e, "iconTabs", 4)}
                    href="#pablo"
                    role="tab"
                  >
                    <i className="ni ni-calendar-grid-58 mr-2" />
                    Расписание
                  </NavLink>
                </NavItem>               
              </Nav>
            </div>
            <Card className="shadow">
              <CardBody>
                <TabContent activeTab={"iconTabs" + this.state.iconTabs}>                  
                  <TabPane tabId="iconTabs1">
                    <p className="description">
                    Инженер — Программист занимаюсь созданием 
      сообщества Образовательного видеохостинга - YouinRoll Контакты для связи:Почта: youinroll@gmail.com
      Все интересующие вас вопросы можно задать мне в сообщениях прямо на моем канале или комментариях.
                    </p>
                  </TabPane>
                  <TabPane tabId="iconTabs2">
                    <VideoCards />
                  </TabPane>
                  <TabPane tabId="iconTabs3">
                    <VideoCards />
                  </TabPane>
                  <TabPane tabId="iconTabs4">
                    <p className="description">
                      пвап
                    </p>
                  </TabPane>
                 
                </TabContent>
              </CardBody>
            </Card>
          </Col>          
        </Row>
      </>
    );
  }
}

export default TabsSection;


